
# home-assistant-podman

Run home-assistant in a rootless podman set up in a restricted access user account.

```console
# adduser home-assistant
# rm /home/home-assistant/.profile 
# chown -R root:root /home/home-assistant/
# loginctl enable-linger `id --user home-assistant` # might not be needed
# mkdir /home/home-assistant/config
# mkdir /home/home-assistant/.cache
# mkdir -p /home/home-assistant/.local/share/containers
# chown home-assistant:home-assistant /home/home-assistant/config
# chown home-assistant:home-assistant /home/home-assistant/.cache 
# chown home-assistant:home-assistant /home/home-assistant/.local/share/containers 
# mkdir /home/home-assistant/logs
# chown root:home-assistant /home/home-assistant/logs
# chmod g+ws /home/home-assistant/logs
# cd /home/home-assistant/
# touch update.sh
# touch start.sh
# chown root:root *.sh
# chmod 0755 *.sh
```
